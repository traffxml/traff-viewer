/*
 * Copyright © 2017–2020 Michael von Glasow.
 * 
 * This file is part of RoadEagle.
 *
 * RoadEagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RoadEagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RoadEagle.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.viewer.core;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentSkipListMap;
import org.traffxml.viewer.util.MessageListener;
import org.traffxml.traff.Traff;
import org.traffxml.traff.TraffMessage;

/* NOTE: 
 * This is a portable class which should be kept clean of any dependencies on platform-specific
 * classes (JRE or Android).
 */

/**
 * Handles storage, retrieval and expiration of TraFF messages.
 * 
 * <p>This class is a weak singleton, which has the following implications:
 * <ul>
 * <li>This class is never instantiated directly. Rather, an instance is obtained by calling
 * {@link #getInstance()}.</li>
 * <li>At any given time, there can never be more than one instance of this class.</li>
 * <li>The instance may be garbage-collected after the last reference to it is deleted. In that
 * case, the next call to {@link #getInstance()} will create a new instance.</li>
 * </ul>
 */
public final class MessageCache {
	/** Milliseconds per minute. */
	private static final int MILLIS_PER_MINUTE = 60000;

	private static WeakReference<MessageCache> instance;

	/** Whether expired messages should be purged. */
	private boolean expirationPolicy = false;

	/** The timer to remove expired messages. */
	private Timer expirationTimer;

	/**
	 * Listeners which get notified whenever new messages are added or removed.
	 */
	private List<WeakReference<MessageListener>> listeners = Collections.synchronizedList(new ArrayList<WeakReference<MessageListener>>());

	/**
	 * In-memory copy of the message cache database.
	 */
	private Map<String, TraffMessage> messages = new ConcurrentSkipListMap<String, TraffMessage>();

	public void clear() {
		synchronized (messages) {
			Set<TraffMessage> messagesToRemove = new TreeSet<TraffMessage>(messages.values());
			messages.clear();
			notifyListeners(null, messagesToRemove);
		}
	}

	/**
	 * Obtains an instance of the MessageCache.
	 * 
	 * @return The instance
	 */
	public static synchronized MessageCache getInstance() {
		MessageCache strongInstance = null;

		if (instance != null) {
			strongInstance = instance.get();
			if (strongInstance != null)
				return strongInstance;
		}

		// need to (re-)instantiate
		strongInstance = new MessageCache();
		instance = new WeakReference<MessageCache>(strongInstance);
		return strongInstance;
	}

	/**
	 * Registers a new {@link MessageListener}.
	 * 
	 * @param listener The new listener
	 */
	public void addListener(MessageListener listener) {
		boolean isInList = false;
		synchronized(listeners) {
			for (WeakReference<MessageListener> ref : listeners)
				if (ref.get() == listener) {
					isInList = true;
					break;
				}
			if (!isInList)
				listeners.add(new WeakReference<MessageListener>(listener));
		}
	}

	/**
	 * Returns the expiration policy for the message cache.
	 * 
	 * @return True if expired messages are purged, false if not
	 */
	public boolean getExpirationPolicy() {
		return expirationPolicy;
	}

	/**
	 * Retrieves a TraFF feed of all messages currently in the cache.
	 * 
	 * @return The TraFF feed as a string
	 * @throws Exception 
	 */
	public String getFeed() throws Exception {
		synchronized (messages) {
			return Traff.createFeed(new ArrayList<TraffMessage>(messages.values()));
		}
	}

	/**
	 * Retrieves a message by its ID.
	 * 
	 * @param id The message ID
	 * @return The message matching the ID, null if no such message exists
	 */
	public TraffMessage getMessage(String id) {
		return messages.get(id);
	}

	/**
	 * Retrieves all messages currently in the cache.
	 */
	public List<TraffMessage> getMessages() {
		synchronized (messages) {
			return new ArrayList<TraffMessage>(messages.values());
		}
	}

	/**
	 * Processes a collection of new TraFF messages.
	 * 
	 * <p>Processing first compiles a list of all messages which are overridden by the new messages. Overridden
	 * messages with a different ID are removed from the list of active messages, and the new message is
	 * added. This replaces any overridden messages with the same ID. Finally, listeners are notified about
	 * the new and deleted messages.
	 * 
	 * @param newMessages The new message
	 */
	// TODO multi-message
	public void putMessages(Collection<TraffMessage> newMessages) {
		/* drop expired messages */
		Date now = new Date();
		if (expirationPolicy) {
			Set<TraffMessage> expired = new TreeSet<TraffMessage>();
			for (TraffMessage message : newMessages)
				if (message.isExpired(now))
					expired.add(message);
			newMessages.removeAll(expired);
		}

		synchronized (messages) {
			Set<TraffMessage> messagesToRemove = new TreeSet<TraffMessage>();
			for (TraffMessage message : newMessages) {
				/* find messages which are overridden by the new message */
				if (messages.containsKey(message.id))
					messagesToRemove.add(messages.get(message.id));
				for (String id : message.replaces)
					if (messages.containsKey(id)) {
						messagesToRemove.add(messages.get(id));
						messages.remove(id);
					}
			}

			for (TraffMessage message : newMessages)
				messages.put(message.id, message);
			notifyListeners(newMessages, messagesToRemove);
		}
	}

	/**
	 * Unregisters a {@link MessageListener}.
	 * 
	 * <p>Unregistering a null listener, or a listener that is not currently registered, is a no-op.
	 * 
	 * <p>This method will also remove from the list any entries pointing to listeners which have been
	 * recycled by the garbage collector.
	 * 
	 * @param listener The new listener
	 */
	public void removeListener(MessageListener listener) {
		List<WeakReference<MessageListener>> toRemove = new ArrayList<WeakReference<MessageListener>>();
		synchronized(listeners) {
			for (WeakReference<MessageListener> ref : listeners)
				if ((ref.get() == listener) || (ref.get() == null))
					toRemove.add(ref);
		}
		for (WeakReference<MessageListener> ref : toRemove)
			listeners.remove(ref);
	}

	/**
	 * Sets the expiration policy for the message cache.
	 * 
	 * @param expirationPolicy True if expired messages are to be purged, false if not
	 */
	public void setExpirationPolicy(boolean expirationPolicy) {
		this.expirationPolicy = expirationPolicy;
	}

	protected void finalize() throws Throwable {
		expirationTimer.cancel();
		super.finalize();
	}

	/**
	 * Instantiates a new MessageCache.
	 * 
	 * <p>This constructor is not intended to be called directly. Instead, obtain an instance by
	 * calling {@link #getInstance()}.
	 */
	private MessageCache() {
		/* expiration timer */
		expirationTimer = new Timer(true);
		expirationTimer.schedule(new ExpirationTimerTask(), 0, MILLIS_PER_MINUTE);
	}

	/**
	 * Notifies all registered listeners about changes to the message cache.
	 * 
	 * @param added A newly added or updated message
	 * @param removed Superseded, canceled or expired messages
	 */
	private void notifyListeners(Collection<TraffMessage> added, Collection<TraffMessage> removed) {
		synchronized(listeners) {
			for (WeakReference<MessageListener> ref : listeners) {
				MessageListener listener = ref.get();
				if (listener != null)
					listener.onUpdateReceived(added, removed);
			}
		}
	}

	private class ExpirationTimerTask extends TimerTask {
		public void run() {
			if (!expirationPolicy)
				return;
			/* purge expired messages */
			synchronized (messages) {
				/* find expired messages */
				Set<TraffMessage> messagesToRemove = new TreeSet<TraffMessage>();
				for (TraffMessage m : messages.values())
					if (m.isExpired(new Date()))
						messagesToRemove.add(m);

				for (TraffMessage m : messagesToRemove)
					messages.remove(m.id);

				notifyListeners(null, messagesToRemove);
			}
		}
	}
}
