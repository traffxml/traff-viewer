package org.traffxml.viewer.input;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.traffxml.traff.TraffFeed;
import org.traffxml.viewer.core.MessageCache;

public class StaticSource {

	/**
	 * Reads TraFF data from memory into the cache.
	 * 
	 * @param feed The TraFF feed to read from
	 * 
	 * @throws Exception
	 */
	public static void readFeed(TraffFeed feed) {
		MessageCache cache = MessageCache.getInstance();
		cache.putMessages(feed.getMessages());
	}

	/**
	 * Reads TraFF data from a file into the cache.
	 * 
	 * @param inputFile The file
	 * 
	 * @throws Exception
	 */
	public static void readFile(File inputFile) throws Exception {
		InputStream dataInputStream = null;
		if (!inputFile.exists())
			throw new IllegalStateException("Input file does not exist.");
		else if (!inputFile.isFile())
			throw new IllegalStateException("Input file is not a file. Aborting.");
		else if (!inputFile.canRead())
			throw new IllegalStateException("Input file is not readable. Aborting.");
		try {
			dataInputStream = new FileInputStream(inputFile);
		} catch (FileNotFoundException e) {
			throw new IllegalStateException("Input file does not exist.", e);
		}
		if (dataInputStream != null) {
			readStream(dataInputStream);
		}
	}

	/**
	 * Reads TraFF data from a file into the cache.
	 * 
	 * @param fileName The path to the file
	 * 
	 * @throws Exception
	 */
	public static void readFile(String fileName) throws Exception {
		File inputFile = new File(fileName);
		readFile(inputFile);
	}

	/**
	 * Reads TraFF data from an input stream into the cache.
	 * 
	 * @param dataInputStream The input stream
	 * 
	 * @throws Exception
	 */
	public static void readStream(InputStream dataInputStream) throws Exception {
		TraffFeed feed = TraffFeed.read(dataInputStream);
		readFeed(feed);
	}
}
