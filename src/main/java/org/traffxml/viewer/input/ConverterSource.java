/*
 * Copyright © 2020–2023 traffxml.org.
 * 
 * This file is part of TraFF Viewer.
 *
 * TraFF Viewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TraFF Viewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TraFF Viewer.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.viewer.input;

import java.io.File;
import java.net.URL;
import java.util.Properties;

import org.traffxml.traff.TraffFeed;

public interface ConverterSource {
	/**
	 * Reads a feed from a file and converts it to TraFF.
	 * 
	 * @param file The file to read and convert
	 * @return A TraFF feed containing all the events in the file
	 * @throws Exception 
	 */
	public TraffFeed convert(File file) throws Exception;

	/**
	 * Reads a feed from a URL and converts it to TraFF.
	 * 
	 * @param url The URL from which to read and convert
	 * @return A TraFF feed containing all the events in the feed
	 * @throws Exception 
	 */
	public TraffFeed convert(URL url) throws Exception;

	/**
	 * Reads a feed specified in a set of properties and converts it to TraFF.
	 * 
	 * How the feed source (often a URL or a file) is specified in the properties depends on the
	 * implementation.
	 * 
	 * @param properties The set of properties
	 * @return A TraFF feed containing all the events in the feed
	 * @throws Exception 
	 */
	public TraffFeed convert(Properties properties) throws Exception;
}
