/*
 * Copyright © 2019–2020 Michael von Glasow.
 * 
 * This file is part of RoadEagle.
 *
 * RoadEagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RoadEagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RoadEagle.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.viewer.input;

import java.io.Closeable;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.traffxml.traff.BoundingBox;
import org.traffxml.traff.TraffFeed;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.subscription.FilterItem;
import org.traffxml.traff.subscription.TraffRequest;
import org.traffxml.traff.subscription.TraffResponse;
import org.traffxml.viewer.core.MessageCache;
import org.traffxml.traff.subscription.TraffRequest.Operation;

/**
 * Encapsulates a TraFF subscription.
 * 
 * <p>When instantiated, this class will attempt to obtain a subscription from the URL passed to the
 * constructor, and poll it periodically.
 * 
 * <p>Currently this class is hardcoded to subscribe to everything (i.e. with a bounding box spanning
 * the whole world and a minimum road class of null), and poll the source every 5 minutes.
 */
public class SubscriptionSource implements Closeable {
	/** The poll interval in seconds */
	private static final int DEFAULT_POLL_INTERVAL = 300;

	private final URL url;
	private String subscriptionId = null;

	/** The timer to poll sources for updates */
	private Timer pollTimer = null;

	public SubscriptionSource(String url) throws Exception {
		super();
		MessageCache cache = MessageCache.getInstance();
		this.url = new URL(url);
		TraffFeed feed = subscribe();
		if ((feed != null) && (feed.getMessages() != null))
			cache.putMessages(feed.getMessages());
	}

	@Override
	public void close() throws IOException {
		if (pollTimer != null)
			pollTimer.cancel();
		if (subscriptionId == null)
			return;
		try {
			TraffResponse response = sendTraffRequest(Operation.UNSUBSCRIBE, subscriptionId, null);
			if (TraffResponse.Status.OK.equals(response.status))
				subscriptionId = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the URL for this source.
	 */
	public URL getUrl() {
		return url;
	}

	/**
	 * Retrieves a list of messages from the source.
	 * 
	 * <p>Sources should return null to indicate failure, or an empty list there are no messages to report.
	 * 
	 * <p>This method allows previously received messages to be specified as an optional argument, in order for
	 * information from those messages to be used in the feed. Sources must ensure that messages do not
	 * simply “disappear” from one feed to the next, as this will cause the old message to remain cached
	 * until it expires. If this is not desired, sources should compare the new feed to the collection of old
	 * messages supplied, and generate cancellations for every message which is no longer valid and not
	 * replaced by a new message.
	 * 
	 * <p>The {@code pollInterval} argument can be used by the source to adjust message expiration times,
	 * ensuring messages will not expire before the next update is received. Sources should avoid returning
	 * messages which are set to expire before an update can be retrieved, unless the condition is known to
	 * end before that time.
	 */
	public Collection<TraffMessage> poll() {
		if (subscriptionId != null) {
			try {
				TraffResponse response = sendTraffRequest(Operation.POLL, subscriptionId, null);
				if (TraffResponse.Status.OK.equals(response.status) && (response.feed != null))
					return response.feed.getMessages();
				else if (!TraffResponse.Status.OK.equals(response.status))
					System.err.println("Source responded with status: " + response.status);
				else if (response.feed == null)
					System.err.println("Source responded with null feed");
				/*
				 * FIXME if response is SUBSCRIPTION_UNKNOWN, try to renew subscription once
				 * (but avoid going into an endless loop)
				 */
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	@Override
	public void finalize() {
		try {
			close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Issues a TraFF request to {@link #url} and returns the result.
	 * 
	 * @param operation The operation requested
	 * @param subscriptionId The subscription ID, if required for the operation, null otherwise
	 * @param filterList A filter list, if setting up or altering a subscription, null otherwise
	 * 
	 * @return The response
	 * 
	 * @throws Exception if anything goes wrong
	 */
	private TraffResponse sendTraffRequest(Operation operation, String subscriptionId,
			List<FilterItem> filterList) throws Exception {
		TraffRequest request = new TraffRequest(operation, subscriptionId, filterList);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "application/xml");
		connection.connect();
		request.write(connection.getOutputStream());
		return TraffResponse.read(connection.getInputStream());
	}

	/**
	 * Subscribes to a TraFF service.
	 * 
	 * @return A feed, if the response included one, or null
	 * @throws Exception 
	 */
	private TraffFeed subscribe() throws Exception {
		TraffFeed feed = null;
		int pollInterval = DEFAULT_POLL_INTERVAL;
		List<FilterItem> filterList = new ArrayList<FilterItem>(1);
		filterList.add(new FilterItem(new BoundingBox(-90, -180, 90, 180), null));
		TraffResponse response = sendTraffRequest(Operation.SUBSCRIBE, null, filterList);
		if (TraffResponse.Status.OK.equals(response.status)
				|| TraffResponse.Status.PARTIALLY_COVERED.equals(response.status)) {
			subscriptionId = response.subscriptionId;
			Integer timeout = response.timeout;
			if ((timeout != null) && (pollInterval * 2 > timeout))
				pollInterval = timeout / 2;
			feed = response.feed;
			pollTimer = new Timer(false);
			/* delay the first poll by one interval if we got a feed already, else poll immediately */
			pollTimer.schedule(new PollTimerTask(),
					(feed == null) ? 0 : pollInterval * 1000,
							pollInterval * 1000);
		}
		else if (!TraffResponse.Status.OK.equals(response.status))
			System.err.println("Subscription failed: source responded with status: " + response.status);
		return feed;
	}

	private class PollTimerTask extends TimerTask {
		MessageCache cache = MessageCache.getInstance();

		@Override
		public void run() {
			/* poll source */
			System.err.println("Polling source");
			try {
				Collection <TraffMessage> newMessages = SubscriptionSource.this.poll();
				if (newMessages != null)
					cache.putMessages(newMessages);
			} catch (Exception e) {
				System.err.println(String.format("Polling source failed with %s",
						e.getClass().getSimpleName()));
				e.printStackTrace();
			}
		}
	}
}
