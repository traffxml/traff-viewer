package org.traffxml.viewer;

import java.awt.EventQueue;

import org.traffxml.viewer.input.ConverterSource;
import org.traffxml.viewer.input.StaticSource;
import org.traffxml.viewer.ui.MainWindow;

public class TraffViewer {
	/**
	 * Launches a new TraffViewer UI.
	 * 
	 * @param converter A converter source
	 */
	public static void launch(ConverterSource converter, String defaultConverterUrl) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow(converter, defaultConverterUrl);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Launches a new TraffViewer UI.
	 */
	public static void launch() {
		launch(null, null);
	}

	public static void main(String[] args) {
		String fileName = null;
		for (int i = 0; i < args.length; i++) {
			if ("-file".equals(args[i])) {
				fileName = getParam("file", args, ++i);
				//flags |= FLAG_FILE;
			} else {
				System.out.println("Unknown argument: " + args[i]);
				System.out.println();
				printUsage();
				System.exit(0);
			}
		}
		if (fileName != null)
			try {
				StaticSource.readFile(fileName);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		launch();
	}

	private static String getParam(String param, String[] args, int pos) {
		if(pos >= args.length) {
			System.out.println("-" + param + " needs an argument.");
			System.exit(1);
		}
		return args[pos];
	}

	private static void printUsage() {
		System.out.println("Arguments:");
		System.out.println("  -file <feed>        Read feed from file <feed>");
	}
}
