/*
 * Copyright © 2017–2020 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.viewer.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.Set;
import org.traffxml.traff.DimensionQuantifier;
import org.traffxml.traff.DurationQuantifier;
import org.traffxml.traff.FrequencyQuantifier;
import org.traffxml.traff.IntQuantifier;
import org.traffxml.traff.IntsQuantifier;
import org.traffxml.traff.PercentQuantifier;
import org.traffxml.traff.Quantifier;
import org.traffxml.traff.SpeedQuantifier;
import org.traffxml.traff.TemperatureQuantifier;
import org.traffxml.traff.TimeQuantifier;
import org.traffxml.traff.TraffEvent;
import org.traffxml.traff.TraffLocation;
import org.traffxml.traff.TraffLocation.Point;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.TraffSupplementaryInfo;
import org.traffxml.traff.WeightQuantifier;

/**
 * Methods for working with TraFF messages.
 */
public class MessageHelper {
	/**
	 * If multiple event classes are present, the priority for the purposes of displaying an icon.
	 */
	private static final List<TraffEvent.Class> CLASS_PRIORITY =
			new ArrayList<TraffEvent.Class>(Arrays.asList(new TraffEvent.Class[]{
					TraffEvent.Class.SECURITY,
					TraffEvent.Class.HAZARD,
					TraffEvent.Class.INCIDENT,
					TraffEvent.Class.WEATHER,
					TraffEvent.Class.EQUIPMENT_STATUS,
					TraffEvent.Class.CONSTRUCTION,
					TraffEvent.Class.DELAY,
					TraffEvent.Class.CONGESTION,
					TraffEvent.Class.RESTRICTION,
					TraffEvent.Class.TRAVEL_TIME,
					TraffEvent.Class.TRANSPORT,
					TraffEvent.Class.CARPOOL,
					TraffEvent.Class.PARKING,
					TraffEvent.Class.ENVIRONMENT,
					TraffEvent.Class.ACTIVITY,
					TraffEvent.Class.AUDIO_BROADCAST,
					TraffEvent.Class.SERVICE
			}));

	/**
	 * The default event comparator.
	 */
	private static final EventComparator EVENT_COMPARATOR = new EventComparator();

	/**
	 * Returns a descriptive string for the detailed location of the message.
	 * 
	 * <p>The detailed location typically refers to a stretch of road or a point on a road, which is
	 * the best approximation of the location of the event.
	 * 
	 * @param message
	 */
	public static String getDetailedLocationName(TraffMessage message) {
		String nameAt = getPointName(message.location.at);
		if (nameAt != null)
			return nameAt;

		String nameFrom = getPointName(message.location.from);
		String nameTo = getPointName(message.location.to);
		if (nameFrom != null) {
			if (nameTo == null) {
				return String.format("%s %s",
						nameFrom,
						TraffLocation.Directionality.ONE_DIRECTION.equals(message.location.directionality) ? "→" : "↔");
			} else if (nameFrom.equals(nameTo))
				return nameFrom;
			else
				return String.format("%s %s %s",
						nameFrom,
						TraffLocation.Directionality.ONE_DIRECTION.equals(message.location.directionality) ? "→" : "↔",
								nameTo);
		} else if (nameTo != null)
			return String.format("%s %s",
					TraffLocation.Directionality.ONE_DIRECTION.equals(message.location.directionality) ? "→" : "↔",
							nameTo);
		else {
			String nameVia = getPointName(message.location.via);
			return nameVia;
		}
	}

	/**
	 * Returns a user-friendly string for the events in the message.
	 * 
	 * @param message
	 */
	public static String getEventText(TraffMessage message) {
		StringBuilder eventBuilder = new StringBuilder();
		for (TraffEvent event : message.events) {
			if ((eventBuilder.length() > 0) && (eventBuilder.charAt(eventBuilder.length() - 1) != '?'))
				eventBuilder.append(". ");
			String eventText = getSingleEventText(event);
			eventText = eventText.substring(0, 1).toUpperCase() + eventText.substring(1);
			eventBuilder.append(eventText);

			for (TraffSupplementaryInfo si : event.supplementaryInfos)
				eventBuilder.append(", " + getSingleSiText(si));

			Integer length = event.length;
			if (length != null)
				try {
					eventBuilder.append(" ").append(getFormattedLength(length));
				} catch (IllegalFormatException e) {
					// TODO Log.w(TAG, "IllegalFormatException while parsing route length string, skipping");
					System.err.println("IllegalFormatException while parsing route length string, skipping");
					e.printStackTrace();
				}

			Integer speed = event.speed;
			if (speed != null)
				try {
					eventBuilder.append(", ").append(getFormattedSpeed(speed));
				} catch (IllegalFormatException e) {
					// TODO Log.w(TAG, "IllegalFormatException while parsing speed limit string, skipping");
					System.err.println("IllegalFormatException while parsing speed limit string, skipping");
					e.printStackTrace();
				}
		}

		if (eventBuilder.charAt(eventBuilder.length() - 1) != '?')
			eventBuilder.append(".");

		if ((message.startTime != null) || (message.endTime != null))
			eventBuilder.append(" ").append(getFormattedDuration(message)).append(".");

		return eventBuilder.toString();
	}

	/**
	 * Returns the message class for a message.
	 * 
	 * <p>The message class is determined based on the update classes of the constituent events of the
	 * message. Conflicts in determining the message class are resolved by applying the following
	 * order of precedence:
	 * <ul>
	 * <li>Security</li>
	 * <li>Warning</li>
	 * <li>Restriction</li>
	 * <li>Info</li>
	 * </ul>
	 */
	public static TraffEvent.Class getMessageClass(TraffMessage message) {
		Set<TraffEvent.Class> classes = new HashSet<TraffEvent.Class>();
		for (TraffEvent event : message.events)
			classes.add(event.eventClass);

		for (TraffEvent.Class eClass : CLASS_PRIORITY)
			if (classes.contains(eClass))
				return eClass;
		/* fallback */
		return TraffEvent.Class.INVALID;
	}

	/**
	 * Returns the service identifier of the message source.
	 * 
	 * @return The name, or null if anything goes wrong.
	 */
	public static String getServiceName(TraffMessage message) {
		try {
			return message.id.substring(0, message.id.indexOf(":"));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Formats time as a relative string.
	 * 
	 * <p>The output can take the following forms:
	 * <ul>
	 * <li>10:15 (today)</li>
	 * <li>Tomorrow 11:00 (yesterday or tomorrow)</li>
	 * <li>Monday 11:00 (up to a week in the past or future)</li>
	 * <li>December 10 (same year; no time given here)</li>
	 * <li>February 8, 2020 (outside the current year)</li>
	 * </ul>
	 * 
	 * @param date The date to format
	 * @return A formatted date
	 */
	public static String formatTime(Date date) {
		if (date == null)
			return null;

		Calendar thenCal = new GregorianCalendar();
		thenCal.setTime(date);
		Calendar nowCal = new GregorianCalendar();

		if (thenCal.get(Calendar.YEAR) == nowCal.get(Calendar.YEAR)
				&& thenCal.get(Calendar.MONTH) == nowCal.get(Calendar.MONTH)
				&& thenCal.get(Calendar.DAY_OF_MONTH) == nowCal.get(Calendar.DAY_OF_MONTH))
			/* today (time only) */
			return DateFormat.getTimeInstance(DateFormat.SHORT).format(date);
		else {
			Calendar thenDayCal = new GregorianCalendar(thenCal.get(Calendar.YEAR), thenCal.get(Calendar.MONTH),
					thenCal.get(Calendar.DAY_OF_MONTH));
			Calendar nowDayCal = new GregorianCalendar(nowCal.get(Calendar.YEAR), nowCal.get(Calendar.MONTH),
					nowCal.get(Calendar.DAY_OF_MONTH));
			long dayDiff = (thenDayCal.getTimeInMillis() - nowDayCal.getTimeInMillis()) / 86400000;

			if (dayDiff == 1) {
				/* tomorrow (with time) */
				return String.format("%s, %s", Const.tomorrow,
						DateFormat.getTimeInstance(DateFormat.SHORT).format(date));
			} else if (dayDiff == -1) {
				/* yesterday (with time) */
				return String.format("%s, %s", Const.yesterday,
						DateFormat.getTimeInstance(DateFormat.SHORT).format(date));
			} else if (Math.abs(dayDiff) < 7) {
				/* within a week: day of week (with time) */
				return String.format("%s, %s", new SimpleDateFormat("EEEE").format(date),
						DateFormat.getTimeInstance(DateFormat.SHORT).format(date));
			} else if (thenCal.get(Calendar.YEAR) == nowCal.get(Calendar.YEAR)) {
				/* same year: month, day */
				//return DateUtils.formatDateTime(date.getTime(), DateUtils.FORMAT_SHOW_DATE);
				// FIXME the above works on Android only. For now, use month/day/year.
				return DateFormat.getDateInstance(DateFormat.LONG).format(date);
			} else
				/* different year: month, day, year */
				return DateFormat.getDateInstance(DateFormat.LONG).format(date);
		}
	}

	public static String toHtml(TraffMessage message) {
		StringBuilder builder = new StringBuilder();
		builder.append("Message ID: <b><tt>").append(message.id).append("</tt></b><br/>");
		builder.append("Receive time: <b>").append(message.getReceiveTimeAsIsoString()).append("</b><br/>");
		builder.append("Update time: <b>").append(message.getUpdateTimeAsIsoString()).append("</b><br/>");
		if (message.expirationTime != null)
			builder.append("Expiration time: <b>").append(message.getExpirationTimeAsIsoString()).append("</b><br/>");
		if (message.startTime != null)
			builder.append("Start time: <b>").append(message.getStartTimeAsIsoString()).append("</b><br/>");
		if (message.endTime != null)
			builder.append("End time: <b>").append(message.getEndTimeAsIsoString()).append("</b><br/>");
		builder.append("Cancellation: <b>").append(message.isCancellation).append("</b><br/>");
		builder.append("Forecast: <b>").append(message.isForecast).append("</b><br/>");
		if (message.urgency != null)
			builder.append("Urgency: <b>").append(message.urgency).append("</b><br/>");

		if ((message.replaces != null) && (message.replaces.length > 0)) {
			builder.append("Replaces:<blockquote>");
			for (String id : message.replaces)
				builder.append("<b><tt>").append(id).append("</tt></b><br/>");
			builder.append("</blockquote>");
		}
		if (message.location != null)
			builder.append(toHtml(message.location));
		if (message.events != null) {
			List<TraffEvent> events = Arrays.asList(message.events);
			events.sort(EVENT_COMPARATOR);
			builder.append("Events:<blockquote>");
			for (TraffEvent event: events)
				builder.append(toHtml(event));
			builder.append("</blockquote>");
		}
		return builder.toString();
	}

	/**
	 * Formats the event duration as a string.
	 * 
	 * @param message The message
	 * @return
	 */
	private static String getFormattedDuration(TraffMessage message) {
		String startText = formatTime(message.startTime);
		String stopText = formatTime(message.endTime);
		if ((startText != null) && (stopText != null)) {
			String duration = String.format(Const.duration, startText, stopText);
			return duration.substring(0, 1).toUpperCase() + duration.substring(1);
		} else if (startText != null) {
			Date now = new Date();
			if (message.startTime.before(now))
				return String.format(Const.since, startText);
			else
				return String.format(Const.from, startText);
		} else if (stopText != null) {
			return String.format(Const.until, stopText);
		} else
			return "";
	}

	/**
	 * Formats the route length as a string.
	 * 
	 * <p>If the length argument exceeds 100, the "for more than 100 km" string will be returned.
	 * 
	 * @param length The length of the route affected, in km
	 * 
	 * @return
	 */
	private static String getFormattedLength(int length) {
		if (length > 100)
			return Const.q_km_max;
		return String.format(Const.q_km, length);
	}

	/**
	 * Formats an event quantifier as a string.
	 * 
	 * <p>Quantifiers cannot be of the {@link IntQuantifier} or {@link IntsQuantifier} class, as these are
	 * processed directly by {@link #getSingleEventText(Context, Event)}.
	 * 
	 * @param q The quantifier to format
	 */
	private static String getFormattedQuantifier(Quantifier q) {
		/* IntQuantifier and IntsQuantifier are handled outside this method. */
		if (q instanceof DimensionQuantifier) {
			return String.format(Const.q_m, ((DimensionQuantifier) q).value);
		} else if (q instanceof DurationQuantifier) {
			// TODO decide whether to use q_duration_min or q_duration_h
			return String.format(Const.q_duration_min, (int) ((DurationQuantifier) q).duration);
		} else if (q instanceof FrequencyQuantifier) {
			// TODO decide whether to use q_freq_khz or q_freq_mhz
			return String.format(Const.q_freq_mhz, ((FrequencyQuantifier) q).frequency);
		} else if (q instanceof PercentQuantifier) {
			return String.format(Const.q_probability, (int) ((PercentQuantifier) q).percent);
		} else if (q instanceof SpeedQuantifier) {
			return String.format(Const.q_km_h, (int) ((SpeedQuantifier) q).speed);
		} else if (q instanceof TemperatureQuantifier) {
			return String.format(Const.q_temperature_celsius, (int) ((TemperatureQuantifier) q).degreesCelsius);
		} else if (q instanceof TimeQuantifier) {
			// TODO
			return "Sorry, not supported yet";
		} else if (q instanceof WeightQuantifier) {
			return String.format(Const.q_t, ((WeightQuantifier) q).weight);
		} else {
			return "ILLEGAL";
		}
	}

	/**
	 * Format the speed as a string.
	 * 
	 * <p>Speed can refer to a speed limit or actual speed of moving traffic; this depends on the event type.
	 * 
	 * @param speed The speed, in km/h
	 */
	private static String getFormattedSpeed(int speed) {
		// TODO distinguish between speed limit and average speed (for display)
		return String.format(Const.q_maxspeed_km_h, speed);
	}

	/**
	 * Returns a descriptive string for a single point of the location.
	 * 
	 * <p>The detailed location can be a junction name (with its reference number, if present), just the
	 * junction number or a kilometric point.
	 */
	private static String getPointName(TraffLocation.Point point) {
		if (point == null)
			return null;
		String res = point.junctionName;
		String junctionRef = point.junctionRef;
		String km = (point.distance != null) ? String.format(Const.distance, point.distance) : null;
		if ((junctionRef != null) && (!junctionRef.isEmpty())) {
			if (res == null)
				res = junctionRef;
			else
				res = res + " (" + junctionRef + ")";
		}
		if (km != null) {
			if (res == null)
				res = km;
			else if (junctionRef == null)
				res = res + " (" + km + ")";
			// TODO find a way to add junction name, ref and kmp
		}
		return res;
	}

	/**
	 * Generates a formatted event description for a single event.
	 * 
	 * <p>If the event has a quantifier, this method returns the description string for the event
	 * with the quantifier parsed and inserted. Otherwise, the generic description is returned.
	 * 
	 * @param event The event
	 * @return The message
	 */
	private static String getSingleEventText(TraffEvent event) {
		return getTextWithQuantifier("event_" + event.type.name().toLowerCase(), event.quantifier);
	}

	/**
	 * Generates a formatted description for a single supplementary information item.
	 * 
	 * <p>If the SI item has a quantifier, this method returns the description string for the item
	 * with the quantifier parsed and inserted. Otherwise, the generic description is returned.
	 * 
	 * @param si The supplementary information item
	 * @return The message
	 */
	private static String getSingleSiText(TraffSupplementaryInfo si) {
		return getTextWithQuantifier(si.type.name().toLowerCase(), si.quantifier);
	}

	/**
	 * Generates a formatted description for a single event or supplementary information item.
	 * 
	 * <p>If the event or SI item has a quantifier, this method returns the description with the quantifier
	 * parsed and inserted. Otherwise, the generic description is returned.
	 * 
	 * <p>The {@code type} argument is the type for the event or SI item. It can be obtained with the expression
	 * {@code event.type.name().toLowerCase()}. For an event, this string must be prefixed with
	 * {@code "event_"}.
	 * 
	 * @param type The event or supplementary information type, see above
	 * @return The message
	 */
	private static String getTextWithQuantifier(String type, Quantifier quantifier) {
		// FIXME expand type into a proper string
		String text = type;
		if (quantifier != null) {
			if (quantifier instanceof IntQuantifier)
				text = String.format("%s, q = %d", text, ((IntQuantifier) quantifier).value);
			else if (quantifier instanceof IntsQuantifier) {
				StringBuilder builder = new StringBuilder();
				String prefix = "(";
				for (int value : ((IntsQuantifier) quantifier).values) {
					builder.append(prefix).append(value);
					prefix = ", ";
				}
				builder.append(")");
				text = String.format("%s, q = %s", text, builder.toString());
			} else
				text = String.format("%s, %s", text, getFormattedQuantifier(quantifier));
		}
		return text;
	}

	private static Object toHtml(TraffEvent event) {
		StringBuilder builder = new StringBuilder();
		builder.append(event.type);
		if (event.quantifier != null) {
			if (event.quantifier instanceof IntQuantifier)
				builder.append(", q: ").append(((IntQuantifier) event.quantifier).value);
			else if (event.quantifier instanceof IntsQuantifier) {
				builder.append(", q: ");
				String prefix = "(";
				for (int value : ((IntsQuantifier) event.quantifier).values) {
					builder.append(prefix).append(value);
					prefix = ", ";
				}
				builder.append(")");
			} else
				builder.append(", q: ").append(getFormattedQuantifier(event.quantifier));
		}
		if (event.length != null)
			builder.append(", length&nbsp;=&nbsp;").append(event.length);
		if (event.probability != null)
			builder.append(", probability&nbsp;=&nbsp;").append(event.probability).append("&nbsp;%");
		if (event.speed != null)
			builder.append(", speed&nbsp;=&nbsp;").append(event.speed);
		if (event.supplementaryInfos != null)
			for (TraffSupplementaryInfo si : event.supplementaryInfos)
				// TODO quantifiers associated with supplementary information
				builder.append(", ").append(si.type);
		builder.append("<br/>");
		return builder.toString();
	}

	private static Object toHtml(TraffLocation location) {
		StringBuilder builder = new StringBuilder("Location:<blockquote>");
		if (location.country != null)
			builder.append("Country: <b>").append(location.country).append("</b><br/>");
		if (location.territory != null)
			builder.append("Territory: <b>").append(location.territory).append("</b><br/>");
		if (location.town != null)
			builder.append("Town: <b>").append(location.town).append("</b><br/>");
		if (location.roadClass != null)
			builder.append("Road class: <b>").append(location.roadClass).append("</b><br/>");
		if (location.roadRef != null)
			builder.append("Road ref: <b>").append(location.roadRef).append("</b><br/>");
		if (location.roadName != null)
			builder.append("Road name: <b>").append(location.roadName).append("</b><br/>");
		if (location.roadIsUrban != null)
			builder.append("Urban: <b>").append(location.roadIsUrban).append("</b><br/>");
		if (location.origin != null)
			builder.append("Origin: <b>").append(location.origin).append("</b><br/>");
		if (location.destination != null)
			builder.append("Destination: <b>").append(location.destination).append("</b><br/>");
		if (location.direction != null)
			builder.append("Direction: <b>").append(location.direction).append("</b><br/>");
		if (location.directionality != null)
			builder.append("Directionality: <b>").append(location.directionality).append("</b><br/>");
		if (location.fuzziness != null)
			builder.append("Fuzziness: <b>").append(location.fuzziness).append("</b><br/>");
		if (location.ramps != null)
			builder.append("Ramps: <b>").append(location.ramps).append("</b><br/>");
		if (location.from != null)
			builder.append(toHtml(location.from, "From", Const.MARKER_FROM));
		if (location.at != null)
			builder.append(toHtml(location.at, "At", Const.MARKER_AT));
		if (location.notVia != null)
			builder.append(toHtml(location.notVia, "Not via", Const.MARKER_NOT_VIA));
		if (location.via != null)
			builder.append(toHtml(location.via, "Via", Const.MARKER_VIA));
		if (location.to != null)
			builder.append(toHtml(location.to, "To", Const.MARKER_TO));
		builder.append("</blockquote>");
		return builder.toString();
	}

	private static Object toHtml(Point point, String role, String marker) {
		StringBuilder builder = new StringBuilder();
		String imgsrc = ClassLoader.getSystemResource(marker).toString();
		builder.append(role).append(": ");
		builder.append("<img src='").append(imgsrc).append("' width=16 height=16/> ");
		builder.append("<b>").append(point.coordinates.lat).append(", ").append(point.coordinates.lon).append("</b><blockquote>");
		if (point.distance != null)
			builder.append("Distance: <b>").append(point.distance).append("</b><br/>");
		if (point.junctionRef != null)
			builder.append("Junction ref: <b>").append(point.junctionRef).append("</b><br/>");
		if (point.junctionName != null)
			builder.append("Junction name: <b>").append(point.junctionName).append("</b><br/>");
		builder.append("</blockquote>");
		return builder.toString();
	}

	private static class EventComparator implements Comparator<TraffEvent> {
		@Override
		public int compare(TraffEvent lhs, TraffEvent rhs) {
			// l < r ==> -1
			int res = 0;
			res = CLASS_PRIORITY.indexOf(lhs.eventClass) - CLASS_PRIORITY.indexOf(rhs.eventClass);
			if (res != 0)
				return res;
			res = lhs.type.compareTo(rhs.type);
			if (res != 0)
				return res;
			// TODO examine speed, quantifiers, probability, supplementary infos
			return res;
		}
	}
}
