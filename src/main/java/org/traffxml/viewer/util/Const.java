/*
 * Copyright © 2020 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.viewer.util;

/**
 * Constants used throughout the application.
 */
public class Const {
	/**
	 * Tile cache name for tiles downloaded from OSM.
	 */
	public static final String TILE_CACHE_OSM = "OSM";

	public static final String MARKER_FROM = "ic_context_marker_green.png";
	public static final String MARKER_VIA = "ic_context_marker_orange.png";
	public static final String MARKER_AT = "ic_context_marker_blue.png";
	public static final String MARKER_TO = "ic_context_marker_red.png";
	public static final String MARKER_NOT_VIA = "ic_context_marker_purple.png";

	/* Message presentation */
    public static final String direction_N = "northbound";
    public static final String direction_NE = "northeastbound";
    public static final String direction_E = "eastbound";
    public static final String direction_SE = "southeastbound";
    public static final String direction_S = "southbound";
    public static final String direction_SW = "southwestbound";
    public static final String direction_W = "westbound";
    public static final String direction_NW = "northwestbound";
    public static final String direction_both = "both directions";
    public static final String distance = "km %.0f";
    public static final String length = "for %d km";
    public static final String length_max = "for more than 100 km";
    public static final String speed = "speed limit %d km/h";
    public static final String since = "Since %s";
    public static final String from = "From %s";
    public static final String until = "Until %s";
    public static final String duration = "%1$s to %2$s";
    public static final String yesterday = "yesterday";
    public static final String today = "today";
    public static final String tomorrow = "tomorrow";
    public static final String mid = "mid-%s";
    public static final String end = "end of %s";
    public static final String q_km = "for %d km";
    public static final String q_km_max = "for more than 100 km";

    /** Speed limit. */
    public static final String q_maxspeed_km_h = "speed %d km/h";

    /** Visibility. */
    public static final String q_visibility_m = "less than %d m";

    /** Probability. */
    public static final String q_probability = "%d %%";

    /** Speed. */
    public static final String q_km_h = "%d km/h";

    /** Duration in minutes. */
    public static final String q_duration_min = "of up to %d minute(s)";

    /** Duration in hours. */
    public static final String q_duration_h = "of up to %d hour(s)";

    /** Temperature in degrees Celsius. */
    public static final String q_temperature_celsius = "%d °C";

    /** Weight in tons. */
    public static final String q_t = "%.1f t";

    /** Dimension in meters. */
    public static final String q_m = "%.1f m";

    /** Precipitation in millimeters. */
    public static final String q_precipitation_mm = "of up to %d mm";

    /** Frequency in MHz. */
    public static final String q_freq_mhz = "%.1f MHz";

    /** Frequency in kHz. */
    public static final String q_freq_khz = "%d kHz";
}
