package org.traffxml.viewer.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JToolBar;
import javax.swing.JButton;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.text.html.HTMLDocument;

import org.mapsforge.core.graphics.Bitmap;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.util.LatLongUtils;
import org.mapsforge.map.awt.graphics.AwtBitmap;
import org.mapsforge.map.awt.graphics.AwtGraphicFactory;
import org.mapsforge.map.awt.util.AwtUtil;
import org.mapsforge.map.awt.view.MapView;
import org.mapsforge.map.layer.LayerManager;
import org.mapsforge.map.layer.Layers;
import org.mapsforge.map.layer.cache.TileCache;
import org.mapsforge.map.layer.download.TileDownloadLayer;
import org.mapsforge.map.layer.download.tilesource.OpenStreetMapMapnik;
import org.mapsforge.map.layer.overlay.Marker;
import org.traffxml.traff.BoundingBox;
import org.traffxml.traff.LatLon;
import org.traffxml.traff.TraffLocation;
import org.traffxml.traff.TraffMessage;
import org.traffxml.viewer.core.MessageCache;
import org.traffxml.viewer.input.ConverterSource;
import org.traffxml.viewer.input.StaticSource;
import org.traffxml.viewer.input.SubscriptionSource;
import org.traffxml.viewer.util.Const;
import org.traffxml.viewer.util.MessageHelper;
import org.traffxml.viewer.util.MessageListener;

import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.Box;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.border.BevelBorder;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.HyperlinkEvent;

public class MainWindow extends JFrame {
	private static final long serialVersionUID = -1303426358875256455L;

	private static String[] MARKERS = {
			Const.MARKER_FROM,
			Const.MARKER_VIA,
			Const.MARKER_AT,
			Const.MARKER_TO,
			Const.MARKER_NOT_VIA
	};

	/** The TraFF message cache. */
	private MessageCache cache = null;

	/** The current path for the file opener dialog. */
	private File currDir = null;

	/** The last URL entered in the conversion dialog. */
	private String lastConvertedUrl = "http://";

	/** The last URL entered in the subscription dialog. */
	private String lastUrl = "http://";

	/** A converter source supplied by the caller. */
	private ConverterSource converter = null;

	/** The current subscription, if we have one. */
	private SubscriptionSource subSource = null;

	/** The Comparator to sort messages in the list. */
	private Comparator<TraffMessage> messageComparator = new DefaultComparator();

	/** A {@link MessageListener} which updates {@code messageAdapter} every time messages are added or deleted. */
	private MessageListener messageListener;

	private JPanel contentPane;
	JButton btnSubscribe;
	JButton btnUnsubscribe;
	private JLabel lblMessageCount;
	private JLabel lblLastOperation;
	private JTable tblList;
	private JEditorPane txtDetails;
	private MapView mapView;
	private Marker[] markerLayers;

	/**
	 * Creates the frame.
	 */
	public MainWindow(ConverterSource converter, String defaultConverterUrl) {
		this.converter = converter;
		this.lastConvertedUrl = defaultConverterUrl;
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				if (subSource != null)
					try {
						subSource.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		});
		setTitle("TraFF Viewer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1024, 700);
		setLocationByPlatform(true);
		setIconImage(Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("ic_launcher.png")));
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JToolBar toolBar = new JToolBar();
		contentPane.add(toolBar, BorderLayout.NORTH);

		JButton btnOpenFile = new JButton("Open file…");
		btnOpenFile.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO set start path
				JFileChooser fc = new JFileChooser(currDir);
				if (fc.showOpenDialog(MainWindow.this) == JFileChooser.APPROVE_OPTION) {
					try {
						currDir = fc.getCurrentDirectory();
						StaticSource.readFile(fc.getSelectedFile());
						lblLastOperation.setText("Read from " + fc.getSelectedFile().getCanonicalPath());
					} catch (Exception e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(MainWindow.this, e1.toString(), "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		toolBar.add(btnOpenFile);

		btnSubscribe = new JButton("Subscribe…");
		btnSubscribe.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String url = (String) JOptionPane.showInputDialog(MainWindow.this,
						"Service URL:", "Subscribe", JOptionPane.PLAIN_MESSAGE, null, null, lastUrl);
				if (url == null)
					return;
				lastUrl = url;
				try {
					subSource = new SubscriptionSource(url);
					btnSubscribe.setEnabled(false);
					btnUnsubscribe.setEnabled(true);
					lblLastOperation.setText("Subscribed to " + url.toString());
				} catch (Exception ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(MainWindow.this, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		toolBar.add(btnSubscribe);

		btnUnsubscribe = new JButton("Unsubscribe");
		btnUnsubscribe.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (subSource != null)
					try {
						subSource.close();
						lblLastOperation.setText("Unsubscribed from " + subSource.getUrl().toString());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				subSource = null;
				btnSubscribe.setEnabled(true);
				btnUnsubscribe.setEnabled(false);
			}
		});
		btnUnsubscribe.setEnabled(false);
		toolBar.add(btnUnsubscribe);

		JButton btnConvertFile = new JButton("Convert file…");
		btnConvertFile.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO set start path
				JFileChooser fc = new JFileChooser(currDir);
				if (fc.showOpenDialog(MainWindow.this) == JFileChooser.APPROVE_OPTION) {
					try {
						currDir = fc.getCurrentDirectory();
						MessageCache cache = MessageCache.getInstance();
						cache.putMessages(MainWindow.this.converter.convert(fc.getSelectedFile()).getMessages());
						lblLastOperation.setText("Converted " + fc.getSelectedFile().getCanonicalPath());
					} catch (Exception e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(MainWindow.this, e1.toString(), "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btnConvertFile.setVisible(this.converter != null);
		toolBar.add(btnConvertFile);

		JButton btnConvertUrl = new JButton("Convert URL…");
		btnConvertUrl.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String url = (String) JOptionPane.showInputDialog(MainWindow.this,
						"Feed URL:", "Convert URL", JOptionPane.PLAIN_MESSAGE, null, null, lastConvertedUrl);
				if (url == null)
					return;
				lastConvertedUrl = url;
				try {
					MessageCache cache = MessageCache.getInstance();
					URL urlAsUrl = new URL(url);
					cache.putMessages(MainWindow.this.converter.convert(urlAsUrl).getMessages());
					lblLastOperation.setText("Converted " + url.toString());
				} catch (Exception ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(MainWindow.this, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnConvertUrl.setVisible(this.converter != null);
		toolBar.add(btnConvertUrl);

		JButton btnClear = new JButton("Clear");
		btnClear.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				cache.clear();
				lblLastOperation.setText("Cleared message cache");
			}
		});
		toolBar.add(btnClear);

		JScrollPane scrollPane = new JScrollPane();

		tblList = new JTable();
		tblList.setModel(new MessageTableModel());
		tblList.getColumnModel().getColumn(1).setPreferredWidth(20);
		tblList.getColumnModel().getColumn(1).setPreferredWidth(330);
		tblList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblList.setDefaultRenderer(String.class, new MultiLineCellRenderer());
		tblList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent evt) {
				int row = tblList.getSelectedRow();
				if (row >= 0) {
					TraffMessage message = ((MessageTableModel) tblList.getModel()).getMessage(row);
					txtDetails.setText(MessageHelper.toHtml(message));

					if (message.location != null) {
						ArrayList<LatLon> coordPairs = new ArrayList<LatLon>();
						TraffLocation.Point[] points = new TraffLocation.Point[] {
							message.location.from,
							message.location.via,
							message.location.at,
							message.location.to,
							message.location.notVia
						};
						for (int i = 0; i < points.length; i++) {
							if (points[i] == null) {
								markerLayers[i].setVisible(false);
								continue;
							}

							coordPairs.add(points[i].coordinates);

							markerLayers[i].setLatLong(new LatLong(points[i].coordinates.lat, points[i].coordinates.lon));
							markerLayers[i].setVisible(true);
						}
						BoundingBox bbox = new BoundingBox(coordPairs);

						mapView.setCenter(new LatLong((bbox.minLat + bbox.maxLat) / 2, (bbox.minLon + bbox.maxLon) / 2));
						if ((bbox.minLat != bbox.maxLat) || (bbox.minLon != bbox.maxLon)) {
							byte zoom = LatLongUtils.zoomForBounds(mapView.getModel().mapViewDimension.getDimension(),
									new org.mapsforge.core.model.BoundingBox(bbox.minLat, bbox.minLon, bbox.maxLat, bbox.maxLon),
									256);
							if (zoom > 17)
								zoom = 17;
							else if (zoom > 0)
								zoom -= 1;
							mapView.setZoomLevel(zoom);
						}
						else
							mapView.setZoomLevel((byte) 16);
					} else {
						mapView.setCenter(new LatLong(0, 0));
						for (Marker marker : markerLayers)
							marker.setVisible(false);
						mapView.setZoomLevel((byte) 0);
					}
				} else {
					txtDetails.setText("");
					mapView.setCenter(new LatLong(0, 0));
					for (Marker marker : markerLayers)
						marker.setVisible(false);
					mapView.setZoomLevel((byte) 0);
				}
			}
		});
		scrollPane.setViewportView(tblList);
		scrollPane.setPreferredSize(new Dimension(360, 400));

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_1.setPreferredSize(new Dimension(360, 400));

		txtDetails = new JEditorPane();
		txtDetails.setContentType("text/html");
		txtDetails.setEditable(false);
		String bodyRule = "body { font-family: Sans-Serif; font-size: 12; }";
		((HTMLDocument) txtDetails.getDocument()).getStyleSheet().addRule(bodyRule);
		scrollPane_1.setViewportView(txtDetails);

		JSplitPane splitPaneLeft = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scrollPane, scrollPane_1);

		mapView = createMapView();

		JSplitPane splitPaneRoot = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, splitPaneLeft, mapView);
		contentPane.add(splitPaneRoot, BorderLayout.CENTER);
		
		JPanel bottomPanel = new JPanel();
		bottomPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		contentPane.add(bottomPanel, BorderLayout.SOUTH);
		bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));

		JLabel lblMessages = new JLabel("Messages: ");
		bottomPanel.add(lblMessages);

		lblMessageCount = new JLabel("0");
		bottomPanel.add(lblMessageCount);

		Component rigidArea = Box.createRigidArea(new Dimension(20, 20));
		bottomPanel.add(rigidArea);

		lblLastOperation = new JLabel("");
		bottomPanel.add(lblLastOperation);

		Component glue = Box.createGlue();
		bottomPanel.add(glue);

		JEditorPane txtAttribution = new JEditorPane();
		txtAttribution.setContentType("text/html");
		txtAttribution.setEditable(false);
		String attrBodyRule = "body { font-family: Dialog; font-size: 12; }";
		((HTMLDocument) txtAttribution.getDocument()).getStyleSheet().addRule(attrBodyRule);
		txtAttribution.addHyperlinkListener(new HyperlinkListener() {
			public void hyperlinkUpdate(HyperlinkEvent event) {
				if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
					try {
						Desktop.getDesktop().browse(event.getURL().toURI());
					} catch (URISyntaxException e) {
						// malformed URI, this should never happen
					} catch (IOException e) {
						System.err.println("Failed to launch browser.");
						e.printStackTrace();
					}
			}
		});
		txtAttribution.setBackground(UIManager.getColor("Panel.background"));
		txtAttribution.setText("<div align=\"right\">Map data © <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors</div>");
		bottomPanel.add(txtAttribution);

		cache = MessageCache.getInstance();

		/* message listener */
		messageListener = new MessageListener() {
			public void onUpdateReceived(final Collection<TraffMessage> added, final Collection<TraffMessage> removed) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						String selectionId = null;
						MessageTableModel model = (MessageTableModel) tblList.getModel();
						if (tblList.getSelectedRow() != -1)
							selectionId = (model.getMessage(tblList.getSelectedRow()).id);
						// TODO do we need to inhibit updates?
						model.clear();
						for (TraffMessage message: cache.getMessages())
							if (!message.isCancellation)
								model.add(message);
						if (model.getRowCount() > 1)
							model.sort(messageComparator);
						model.refresh();
						if (selectionId != null) {
							int index = model.indexOf(cache.getMessage(selectionId));
							if (index >= 0) {
								tblList.setRowSelectionInterval(index, index);
								// FIXME this does not work as expected
								tblList.scrollRectToVisible(tblList.getCellRect(index, 0, true));
							} else
								tblList.clearSelection();
						}
						lblMessageCount.setText(String.format("%d", model.getRowCount()));
					}
				});
			}
		};

		cache.addListener(messageListener);

		/* fire the message listener once to get initial cache contents */
		messageListener.onUpdateReceived(null, null);
	}

	public MainWindow() {
		this(null, null);
	}

	private MapView createMapView() {
		MapView res = new MapView();
		res.getMapScaleBar().setVisible(true);

		// TODO can we determine the version string dynamically?
		OpenStreetMapMapnik tileSource = OpenStreetMapMapnik.INSTANCE;
		tileSource.setUserAgent(String.format("%s/%s (%s)", "TraFF Viewer", "0.0.1-SNAPSHOT",
				System.getProperty("http.agent")));

		TileCache tileCache = AwtUtil.createTileCache(
				256,
				res.getModel().frameBufferModel.getOverdrawFactor(),
				1024,
				new File(System.getProperty("java.io.tmpdir"), Const.TILE_CACHE_OSM));

		LayerManager layerManager = res.getLayerManager();
		Layers layers = layerManager.getLayers();

		TileDownloadLayer mapDownloadLayer = new TileDownloadLayer(tileCache,
				res.getModel().mapViewPosition, tileSource,
				AwtGraphicFactory.INSTANCE);
		layers.add(0, mapDownloadLayer);
		mapDownloadLayer.start();

		markerLayers = new Marker[MARKERS.length];
		for (int i = 0; i < MARKERS.length; i++) {
			try {
				Bitmap bitmap = new AwtBitmap(ClassLoader.getSystemResourceAsStream(MARKERS[i]));
				Marker marker = new Marker(new LatLong(0, 0),
						bitmap, 0, -bitmap.getHeight() * 10 / 24);
				layers.add(marker);
				markerLayers[i] = marker;
				marker.setVisible(false);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		res.setCenter(new LatLong(0, 0));
		res.setZoomLevel((byte) 0);

		return res;
	}

	private class MessageTableModel extends DefaultTableModel {
		private static final long serialVersionUID = -3983593371938595752L;
		private ArrayList<TraffMessage> messages = new ArrayList<TraffMessage>();

		public void add(TraffMessage message) {
			messages.add(message);
		}

		public void clear() {
			messages.clear();
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			return String.class;
		}

		@Override
		public int getColumnCount() {
			return 2;
		}

		@Override
		public String getColumnName(int column) {
			switch(column) {
			case 0: return "Road ref";
			case 1: return "Description";
			default: return "ERR";
			}
		}

		public TraffMessage getMessage(int selectedRow) {
			return messages.get(selectedRow);
		}

		@Override
		public int getRowCount() {
			/* during initialization, messages is not set yet */
			if (messages == null)
				return 0;
			return messages.size();
		}

		@Override
		public Object getValueAt(int row, int column) {
			String res = "";
			TraffMessage message = messages.get(row);
			// TODO implement
			switch(column) {
			case 0:
				if (message.location != null) {
					if (message.location.country != null)
						res = message.location.country;
					if ((message.location.roadRef != null) && !message.location.roadRef.isEmpty())
						res = res + (res.isEmpty() ? "" : "\n") + message.location.roadRef;
				}
				return res;
			case 1:
				/* roadName: Road or location name */
				String direction = null;
				if (TraffLocation.Directionality.ONE_DIRECTION.equals(message.location.directionality)) {
					/* determine bearing between from and to; if exactly one of them is null, try at instead */
					TraffLocation.Point loc1 = message.location.from;
					TraffLocation.Point loc2 = message.location.to;
					if ((loc1 == null) && (loc2 != null))
						loc1 = message.location.at;
					else if ((loc1 != null) && (loc2 == null))
						loc2 = message.location.at;
					if ((loc1 != null) && (loc2 != null)) {
						double bearing = loc1.coordinates.bearingTo(loc2.coordinates);
						if ((bearing >= 330) || (bearing <= 30))
							direction = Const.direction_N;
						else if (bearing < 60)
							direction = Const.direction_NE;
						else if (bearing <= 120)
							direction = Const.direction_E;
						else if (bearing < 150)
							direction = Const.direction_SE;
						else if (bearing <= 210)
							direction = Const.direction_S;
						else if (bearing < 240)
							direction = Const.direction_SW;
						else if (bearing <= 300)
							direction = Const.direction_W;
						else
							direction = Const.direction_NW;
					}
				} else
					direction = Const.direction_both;
				if ((message.location.roadName != null) && !message.location.roadName.isEmpty()) {
					/* roadName specified: roadName, town, direction */
					res = (message.location.town == null) ? "" : (message.location.town + ", ");
					// TODO territory?
					if (direction != null)
						res = res + message.location.roadName + ", " + direction;
					else
						res = res + message.location.roadName;
				} else if ((message.location.origin != null) && !message.location.origin.isEmpty()
						&& (message.location.destination != null) && !message.location.destination.isEmpty()) {
					/* origin and destination specified: origin↔destination or origin→destination */
					res = message.location.origin
							+ (TraffLocation.Directionality.ONE_DIRECTION.equals(message.location.directionality) ? " → " : " ↔ ")
							+ message.location.destination;
				} else if (((message.location.origin == null) || message.location.origin.isEmpty())
						&& ((message.location.destination == null) || message.location.destination.isEmpty())) {
					/* no origin and no destination: direction, if available */
					if (direction != null)
						res = direction.substring(0, 1).toUpperCase() + direction.substring(1);
				} else if (TraffLocation.Directionality.ONE_DIRECTION.equals(message.location.directionality)) {
					/* unidirectional, either origin or destination: replace the missing one with the direction */
					if ((message.location.origin != null) && !message.location.origin.isEmpty())
						res = message.location.origin + " → " + ((direction != null) ? direction: "");
					else
						res = ((direction != null) ? (direction.substring(0, 1).toUpperCase() + direction.substring(1)) : "") +
						" → " + message.location.destination;
				} else {
					/* bidirectional, only origin or only destination: no meaningful way to use the name */
					if (direction != null)
						res = direction.substring(0, 1).toUpperCase() + direction.substring(1);
				}

				/* locationName: Detailed location ("at place", "between place1 and place2") */
				String locationNameText = MessageHelper.getDetailedLocationName(message);
				if (locationNameText != null)
					res = res + (res.isEmpty() ? "" : "\n") + locationNameText;

				/* eventDescription */
				String eventText = MessageHelper.getEventText(message);
				if (eventText != null)
					res = res + (res.isEmpty() ? "" : "\n") + eventText;

				/* source, updated */
				res = res + (res.isEmpty() ? "" : "\n") + MessageHelper.getServiceName(message) +
						"\t" + MessageHelper.formatTime(message.updateTime);
				return res;
			default:
				return "ERR";
			}
		}

		public int indexOf(TraffMessage message) {
			return messages.indexOf(message);
		}

		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}

		public void refresh() {
			this.fireTableDataChanged();
		}

		public void sort(Comparator<TraffMessage> messageComparator) {
			messages.sort(messageComparator);
		}
	}

	private class MultiLineCellRenderer extends JTextArea implements TableCellRenderer {
		private static final long serialVersionUID = -3015093693443532559L;
		private final Map<Integer, Map<Integer, Integer>> cellSizes = new HashMap<Integer, Map<Integer, Integer>>();

		public MultiLineCellRenderer() {
			setLineWrap(true);
			setWrapStyleWord(true);
			setOpaque(true);
			setBorder(new EmptyBorder(-1, 2, -1, 2));
			setRows(1);
		}

		public Component getTableCellRendererComponent(JTable table, Object value,
				boolean isSelected, boolean hasFocus, int row, int column) {
			String text = value == null ? "" : value.toString();
			if (!getText().equals(text)) {
				setText(text);

			    TableColumnModel columnModel = table.getColumnModel();
			    setSize(columnModel.getColumn(column).getWidth(), 100000);
			    addSize(row, column, (int) getPreferredSize().getHeight());
			    int newHeight = getRowHeight(row);
				if (table.getRowHeight(row) != newHeight)
					table.setRowHeight(row, newHeight);
			}

			if (isSelected) {
				setForeground(table.getSelectionForeground());
				setBackground(table.getSelectionBackground());
			}
			else {
				setForeground(table.getForeground());
				setBackground(table.getBackground());
			}

			return this;
		}

		private void addSize(int row, int column, int height) {
			Map<Integer, Integer> rowHeights = cellSizes.get(new Integer(row));
			if (rowHeights == null) {
				cellSizes.put(new Integer(row), rowHeights = new HashMap<Integer, Integer>());
			}
			rowHeights.put(new Integer(column), new Integer(height));
		}

		private int getRowHeight(int row) {
			Map<Integer, Integer> rowHeights = cellSizes.get(new Integer(row));
			if (rowHeights == null)
				return getRowHeight();
			int res = 0;
			for (Iterator<Map.Entry<Integer, Integer>> iter = rowHeights.entrySet().iterator(); iter.hasNext(); ) {
				Map.Entry<Integer, Integer> entry = iter.next();
				int cellHeight = entry.getValue().intValue();
				res = Math.max(res, cellHeight);
			}
			return res;
		}
	}

	/**
	 * The default comparator for sorting TraFF messages.
	 * 
	 * <p>This comparator compares events, using the following items of information in the order
	 * shown, until a difference is found:
	 * <ol>
	 * <li>Road class</li>
	 * <li>Road numbers</li>
	 * <li>Country</li>
	 * <li>(not yet implemented) road name</li>
	 * <li>(not yet implemented) directionality and direction</li>
	 * <li>(not yet implemented) junction numbers</li>
	 * <li>(not yet implemented) kilometric points</li>
	 * <li>(not yet implemented) coordinates</li>
	 * </ol>
	 * 
	 * <p>Road numbers are sorted using a {@link NumberStringComparator}; countries are sorted lexicographically.
	 * 
	 * <p>When sorting, null values are placed at the end, two null values are considered equal (causing the
	 * next items in the above list to be examined).
	 */
	public static class DefaultComparator implements Comparator<TraffMessage> {
		private NumberStringComparator nsc = new NumberStringComparator();

		/**
		 * @return -1 if {@code lhs} appears first as per the sort order, 1 if {@code rhs} appears
		 * first, 0 if no order can be determined 
		 */
		public int compare(TraffMessage lhs, TraffMessage rhs) {
			int res = 0;
			/* compare by road class */
			if ((lhs.location != null) && (lhs.location.roadClass != null)
					&& (rhs.location != null) && (rhs.location.roadClass != null))
				res = lhs.location.roadClass.compareTo(rhs.location.roadClass);
			else if ((lhs.location != null) && (lhs.location.roadClass != null))
				res = -1;
			else if ((rhs.location != null) && (rhs.location.roadClass != null))
				res = 1;
			if (res != 0)
				return res;

			// TODO attempt to separate directions

			/* compare by road numbers (if only one location has a road number, it is first) */
			String lrn = (lhs.location == null) ? null : lhs.location.roadRef;
			String rrn = (rhs.location == null) ? null : rhs.location.roadRef;
			if ((lrn != null) && (rrn != null)) {
				res = nsc.compare(lrn, rrn);
				if (res != 0)
					return res;
			} else if (lrn != null)
				return -1;
			else if (rrn != null)
				return 1;

			/* compare by country (if only one location has a country, it is first) */
			String lc = (lhs.location == null) ? null : lhs.location.country;
			String rc = (rhs.location == null) ? null : rhs.location.country;
			if ((lc != null) && (rc != null)) {
				res = lc.compareTo(rc);
				if (res != 0)
					return res;
			} else if (lc != null)
				return -1;
			else if (rc != null)
				return 1;

			// TODO compare by road name

			// TODO sort by directionality and direction (first forward, then bidirectional, then backward)

			// TODO sort by junction numbers

			// TODO sort by kilometric points, once we have support

			// TODO compare by coordinates

			return 0;
		}
	}

	/**
	 * A comparator for strings containing numbers.
	 *
	 * <p>To compare two strings, each string is broken down into numeric and non-numeric parts.
	 * Then both string are compared part by part. The following rules apply:
	 * <ul>
	 * <li>Whitespace bordering on a number is discarded</li>
	 * <li>Whitespace surrounded by string characters is treated as one string with the surrounding
	 * characters</li>
	 * <li>If one string has more parts than the other, the shorter string is padded with null
	 * parts.</li>
	 * <li>Null is less than non-null.</li>
	 * <li>Null equals null.</li>
	 * <li>A numeric part is less than a string part.</li>
	 * <li>Numeric parts are compared as integers.</li>
	 * <li>String parts are compared as strings.</li>
	 * </ul>
	 * 
	 * <p>If one of the strings supplied is null or empty (but not the other), it is considered to be
	 * greater than the other, causing it to be inserted at the end.
	 */
	public static class NumberStringComparator implements Comparator<String> {
		private enum State { WHITESPACE, NUMERIC, ALPHA };

		/**
		 * @return -1 if {@code lhs} appears first as per the sort order, 1 if {@code rhs} appears
		 * first, 0 if no order can be determined 
		 */
		public int compare(String lhs, String rhs) {
			int res = 0;
			if ((lhs == null) || lhs.isEmpty()) {
				if ((rhs == null) || rhs.isEmpty())
					return 0;
				else
					return 1;
			} else if ((rhs == null) || rhs.isEmpty())
				return -1;

			int i;
			List<Object> l = parse(lhs);
			List<Object> r = parse(rhs);
			for (i = 0; i < Math.min(l.size(), r.size()); i++) {
				if (l.get(i) instanceof Integer) {
					if (r.get(i) instanceof Integer)
						res = (Integer) l.get(i) - (Integer) r.get(i);
					else if (r.get(i) instanceof String)
						return -1;
				} else if (l.get(i) instanceof String) {
					if (r.get(i) instanceof Integer)
						return 1;
					else if (r.get(i) instanceof String)
						res = ((String) l.get(i)).compareTo((String) r.get(i));
				}
				if (res != 0)
					return res;
			}

			if (i < l.size())
				return 1;
			else if (i < r.size())
				return -1;
			else
				return 0;
		}

		/**
		 * Parses a string into numeric and non-numeric components.
		 * @param string The string to parse
		 * @return A list of {@link String} and {@link Integer} objects.
		 */
		private List<Object> parse(String string) {
			List<Object> res = new LinkedList<Object>();
			State state = State.WHITESPACE;
			int i = 0;

			while (i < string.length()) {
				char c = string.charAt(i);
				if (c <= 0x20) {
					/* whitespace */
					if (state == State.NUMERIC) {
						res.add(Integer.valueOf(string.substring(0, i)));
						string = string.substring(i);
						i = 1;
						state = State.WHITESPACE;
					} else
						i++;
				} else if ((c >= '0') && (c <= '9')) {
					/* numeric */
					if (state == State.ALPHA) {
						res.add(string.substring(0, i).trim());
						string = string.substring(i);
						i = 1;
					} else
						i++;
					state = State.NUMERIC;
				} else {
					/* alpha */
					if (state == State.NUMERIC) {
						res.add(Integer.valueOf(string.substring(0, i)));
						string = string.substring(i);
						i = 1;
					} else
						i++;
					state = State.ALPHA;
				}
			}

			if (string.length() > 0) {
				if (state == State.NUMERIC)
					res.add(Integer.valueOf(string));
				else if (state == State.ALPHA)
					res.add(string.trim());
			}

			return res;
		}
	}
}
