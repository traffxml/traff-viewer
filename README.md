The API documentation for `master` can be found at https://traffxml.gitlab.io/traff-viewer/javadoc/master/.

The API documentation for `dev` can be found at https://traffxml.gitlab.io/traff-viewer/javadoc/dev/.